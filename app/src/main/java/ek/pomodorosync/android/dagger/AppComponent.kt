package ek.pomodorosync.android.dagger

import dagger.Component
import ek.pomodorosync.android.data.TimerService
import ek.pomodorosync.android.ui.activity.MainActivity
import ek.pomodorosync.android.ui.fragment.auth.AuthFragment
import ek.pomodorosync.android.ui.fragment.history.HistoryFragment
import ek.pomodorosync.android.ui.fragment.reports.ReportsFragment
import ek.pomodorosync.android.ui.fragment.tasks.AddTaskFragment
import ek.pomodorosync.android.ui.fragment.tasks.TaskListFragment
import ek.pomodorosync.android.ui.fragment.timer.TimerFragment
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class, NavigationModule::class, BindsModule::class))
interface AppComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(authFragment: AuthFragment)
    fun inject(timerFragment: TimerFragment)
    fun inject(taskListFragment: TaskListFragment)
    fun inject(addTaskFragment: AddTaskFragment)
    fun inject(timerService: TimerService)
    fun inject(reportsFragment: ReportsFragment)
    fun inject(historyFragment: HistoryFragment)
}
