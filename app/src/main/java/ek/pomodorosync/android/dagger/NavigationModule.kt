package ek.pomodorosync.android.dagger

import dagger.Module
import dagger.Provides
import ek.pomodorosync.common.AppRouter
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import javax.inject.Singleton

@Module
class NavigationModule {

    private var cicerone = Cicerone.create(AppRouter())

    @Provides @Singleton
    fun provideRouter(): AppRouter = cicerone.router

    @Provides @Singleton
    fun provideNavigatorHolder(): NavigatorHolder = cicerone.navigatorHolder
}
