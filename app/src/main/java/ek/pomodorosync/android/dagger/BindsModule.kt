package ek.pomodorosync.android.dagger

import dagger.Binds
import dagger.Module
import ek.pomodorosync.android.data.AndroidAuthRepository
import ek.pomodorosync.android.data.AndroidTasksRepository
import ek.pomodorosync.android.data.AndroidTimerRepository
import ek.pomodorosync.common.data.AuthRepository
import ek.pomodorosync.common.data.TasksRepository
import ek.pomodorosync.common.data.TimerRepository
import javax.inject.Singleton

@Module
abstract class BindsModule {

    @Binds @Singleton
    abstract fun provideAuthRepository(repository: AndroidAuthRepository): AuthRepository

    @Binds @Singleton
    abstract fun provideTasksRepository(repository: AndroidTasksRepository): TasksRepository

    @Binds @Singleton
    abstract fun provideTimerRepository(repository: AndroidTimerRepository): TimerRepository
}