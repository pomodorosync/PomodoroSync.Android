package ek.pomodorosync.android.dagger

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(val applicationContext: Context) {

    @Provides @Singleton
    fun provideApplicationContext(): Context = applicationContext
}