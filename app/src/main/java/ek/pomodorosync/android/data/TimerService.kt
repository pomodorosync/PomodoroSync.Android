package ek.pomodorosync.android.data

import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.support.v4.app.NotificationManagerCompat
import android.support.v7.app.NotificationCompat
import ek.pomodorosync.R
import ek.pomodorosync.android.App
import ek.pomodorosync.common.data.TimerRepository
import ek.pomodorosync.common.data.models.TimerState
import ek.pomodorosync.common.util.CommonSchedulers
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class TimerService : Service() {
    companion object {
        const val ACTION_START = "ek.pomodorosync.android.ACTION_START"
        const val ACTION_STOP = "ek.pomodorosync.android.ACTION_STOP"

        private const val NOTIFICATION_ID = 1
    }

    @Inject
    lateinit var timerRepository: TimerRepository

    private var onDestroyDisposables = CompositeDisposable()

    private var currentTimerState: TimerState? = null

    override fun onCreate() {
        App.instance.appComponent.inject(this)
        super.onCreate()

        timerRepository.getTimerState()
                .observeOn(CommonSchedulers.mainThread())
                .subscribeBy(onNext = {
                    onStateChanged(it)
                }, onError = { e ->
                    when (e) {
                        is UserNotAuthorizedException -> stopSelf()
                    }
                })
                .addTo(onDestroyDisposables)


        Observable.interval(10, TimeUnit.SECONDS, CommonSchedulers.mainThread())
                .subscribe { updateNotification() }
                .addTo(onDestroyDisposables)
    }


    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        when (intent.action) {
            ACTION_STOP -> {
                timerRepository.stopTimer()
            }
        }
        return START_NOT_STICKY
    }

    override fun onBind(intent: Intent) = null

    override fun onDestroy() {
        onDestroyDisposables.dispose()
        super.onDestroy()
    }

    private fun onStateChanged(state: TimerState) {
        if (state.isRunning) {
            showNotification(state)
        } else {
            stopSelf()
        }
        currentTimerState = state
    }

    private fun showNotification(state: TimerState) {
        startForeground(NOTIFICATION_ID, buildNotification(state))
    }

    private fun updateNotification() {
        val state = currentTimerState ?: return

        NotificationManagerCompat.from(this).notify(NOTIFICATION_ID, buildNotification(state))
    }

    private fun buildNotification(state: TimerState): Notification? {
        val startInstant = Instant.ofEpochMilli(state.startTime)
        val duration = Duration.ofMillis(state.duration)
        val startTime = LocalDateTime.ofInstant(startInstant, ZoneId.systemDefault())
        val endTime = startTime.plus(duration)
        val elapsedTime = Duration.between(startTime, LocalDateTime.now())

        return NotificationCompat.Builder(this).apply {
            setContentTitle("Очень важная задача")
            setContentText("Рабочая сессия до ${endTime.format(DateTimeFormatter.ofPattern("HH:mm"))}")
            setProgress(duration.seconds.toInt(), elapsedTime.seconds.toInt(), false)

            val stopIntent = Intent(this@TimerService, TimerService::class.java)
            stopIntent.action = ACTION_STOP

            addAction(R.drawable.ic_stop_black_24dp, "Отменить", PendingIntent.getService(this@TimerService, 0, stopIntent, 0))

            setSmallIcon(R.mipmap.ic_launcher)
            color = 0xFFEF0000.toInt()
            setSound(null)
        }.build()
    }
}
