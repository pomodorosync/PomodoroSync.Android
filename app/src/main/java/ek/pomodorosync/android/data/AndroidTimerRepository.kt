package ek.pomodorosync.android.data

import android.content.Context
import android.content.Intent
import com.google.firebase.database.FirebaseDatabase
import ek.pomodorosync.android.util.rxValueChanges
import ek.pomodorosync.android.util.toCompletable
import ek.pomodorosync.common.data.TimerRepository
import ek.pomodorosync.common.data.models.TimerState
import io.reactivex.Completable
import io.reactivex.Observable
import org.threeten.bp.Instant
import javax.inject.Inject

class AndroidTimerRepository @Inject constructor(
        private val context: Context,
        private val authRepository: AndroidAuthRepository
) : TimerRepository {
    private val db = FirebaseDatabase.getInstance()

    override fun startTimer(duration: Long, isBreak: Boolean): Completable {
        return setTimerState(TimerState(isBreak, true, Instant.now().toEpochMilli(), duration))
    }

    override fun stopTimer(): Completable {
        val userId = authRepository.userId ?: return Completable.error(UserNotAuthorizedException())
        return db.reference
                .child("timer_state")
                .child(userId)
                .child("isRunning")
                .setValue(false)
                .toCompletable()
    }

    override fun getTimerState(): Observable<TimerState> {
        val userId = authRepository.userId ?: return Observable.error(UserNotAuthorizedException())


        return db.reference
                .child("timer_state")
                .child(userId)
                .rxValueChanges()
                .map { TimerState.fromMap(it.value as? Map<*, *>) }
                .doOnNext {
                    if (it.isRunning) {
                        context.startService(Intent(context, TimerService::class.java))
                    }
                }
    }

    override fun setTimerState(state: TimerState): Completable {
        val userId = authRepository.userId ?: return Completable.error(UserNotAuthorizedException())

        return db.reference
                .child("timer_state")
                .child(userId)
                .setValue(state.toMap())
                .toCompletable()
    }
}