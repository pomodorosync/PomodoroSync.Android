package ek.pomodorosync.android.data

import com.google.firebase.auth.FirebaseAuth
import ek.pomodorosync.common.data.AuthRepository
import ek.pomodorosync.common.data.models.UserInfo
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.disposables.Disposables
import javax.inject.Inject

class AndroidAuthRepository @Inject constructor(
) : AuthRepository {
    override val isLoggedIn: Boolean
        get() = FirebaseAuth.getInstance().currentUser != null

    override val userId: String?
        get() = FirebaseAuth.getInstance().currentUser?.uid

    override val userInfo: UserInfo?
        get() = FirebaseAuth.getInstance().currentUser?.let {
            UserInfo(it.displayName, it.email, it.photoUrl?.toString())
        }

    override fun isLoggedInChanges(): Observable<Boolean> {
        return Observable.create { emitter ->
            val authStateListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
                if (!emitter.isDisposed) {
                    val user = firebaseAuth.currentUser
                    emitter.onNext(user != null)
                }
            }

            FirebaseAuth.getInstance().addAuthStateListener(authStateListener)

            emitter.setDisposable(Disposables.fromAction {
                FirebaseAuth.getInstance().removeAuthStateListener(authStateListener)
            })
        }
    }

    override fun login(email: String, password: String): Completable {
        return Completable.create { emitter ->
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                    .addOnSuccessListener {
                        if (!emitter.isDisposed) emitter.onComplete()
                    }
                    .addOnFailureListener { e ->
                        if (!emitter.isDisposed) emitter.onError(e)
                    }
        }
    }

    override fun loginAnonymously(): Completable {
        return Completable.create { emitter ->
            FirebaseAuth.getInstance().signInAnonymously()
                    .addOnSuccessListener {
                        if (!emitter.isDisposed) emitter.onComplete()
                    }
                    .addOnFailureListener { e ->
                        if (!emitter.isDisposed) emitter.onError(e)
                    }
        }
    }

    override fun signUp(email: String, password: String): Completable {
        return Completable.create { emitter ->
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                    .addOnSuccessListener {
                        if (!emitter.isDisposed) emitter.onComplete()
                    }
                    .addOnFailureListener { e ->
                        if (!emitter.isDisposed) emitter.onError(e)
                    }
        }
    }

    override fun logout(): Completable {
        FirebaseAuth.getInstance().signOut()
        return Completable.complete()
    }
}