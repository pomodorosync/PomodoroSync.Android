package ek.pomodorosync.android.data

class UserNotAuthorizedException() : RuntimeException()