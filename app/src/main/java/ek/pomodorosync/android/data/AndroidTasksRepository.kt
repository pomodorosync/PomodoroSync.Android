package ek.pomodorosync.android.data

import com.google.firebase.database.FirebaseDatabase
import ek.pomodorosync.android.util.rxValueChanges
import ek.pomodorosync.android.util.toCompletable
import ek.pomodorosync.common.data.AuthRepository
import ek.pomodorosync.common.data.TasksRepository
import ek.pomodorosync.common.data.models.TaskModel
import ek.pomodorosync.common.domain.TaskPriority
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject

class AndroidTasksRepository @Inject constructor(
        private val authRepository: AuthRepository
) : TasksRepository {
    private val database = FirebaseDatabase.getInstance()

    override fun getTasks(): Observable<List<TaskModel>> {
        val userId = authRepository.userId ?: return Observable.error(UserNotAuthorizedException())

        return database.reference
                .child("tasks")
                .child(userId)
                .rxValueChanges()
                .map { dataSnapshot ->
                    dataSnapshot.children.map { child -> TaskModel.fromMap(child.value as Map<*, *>) }
                }
    }

    override fun addTask(name: String, priority: TaskPriority): Completable {
        val userId = authRepository.userId ?: return Completable.error(UserNotAuthorizedException())

        val value = TaskModel(name)

        return database.reference
                .child("tasks")
                .child(userId)
                .push()
                .setValue(value)
                .toCompletable()
    }
}
