package ek.pomodorosync.android.ui.fragment.timer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import ek.pomodorosync.R
import ek.pomodorosync.android.App
import ek.pomodorosync.android.ui.fragment.BaseFragment
import ek.pomodorosync.android.util.format
import ek.pomodorosync.android.util.setVisible
import ek.pomodorosync.common.presentation.timer.TimerPresenter
import ek.pomodorosync.common.presentation.timer.TimerView
import kotlinx.android.synthetic.main.fragment_timer.*
import org.threeten.bp.Duration
import javax.inject.Inject
import javax.inject.Provider

class TimerFragment : BaseFragment(), TimerView {

    @InjectPresenter
    lateinit var presenter: TimerPresenter

    //region Presenter injection
    @Inject lateinit var presenterProvider: Provider<TimerPresenter>

    @ProvidePresenter fun providePresenter() = presenterProvider.get()!!
    //endregion

    override fun onCreate(savedInstanceState: Bundle?) {
        App.instance.appComponent.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_timer, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbarNavigation(btnMenu)
        initViews()
    }

    private fun initViews() {
        btnStart.setOnClickListener {
            presenter.startTimer()
        }
        btnStop.setOnClickListener { presenter.stopTimer() }

        btnSettings.setOnClickListener {
            presenter.logout()
        }
    }

    override fun setAvailableActions(vararg availableActions: TimerView.Action) {
        for (action in TimerView.Action.values()) {
            val isAvailable = action in availableActions
            when (action) {
                TimerView.Action.START -> btnStart.setVisible(isAvailable)
                TimerView.Action.STOP -> btnStop.setVisible(isAvailable)
            }
        }
    }

    override fun setRemainingTime(time: Long) {
        val duration = Duration.ofMillis(time)
        tvTimer.text = duration.format()
    }
}