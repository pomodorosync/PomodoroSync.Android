package ek.pomodorosync.android.ui.fragment

import android.support.v7.widget.Toolbar
import android.widget.ImageView
import com.arellomobile.mvp.MvpAppCompatFragment
import ek.pomodorosync.R
import ek.pomodorosync.android.ui.fragment.timer.TimerFragment
import ek.pomodorosync.common.AppRouter
import javax.inject.Inject

open class BaseFragment : MvpAppCompatFragment() {

    @Inject lateinit var router: AppRouter

    companion object {
        const val ARG_TOOLBAR_ACTION = "ARG_TOOLBAR_ACTION"
        const val TOOLBAR_ACTION_MENU = "menu"
        const val TOOLBAR_ACTION_BACK = "back"
    }

    val menuIcon by lazy { if (this is TimerFragment) R.drawable.ic_menu_black_24dp else R.drawable.ic_menu_white_24dp }
    val backIcon = R.drawable.ic_back_white_24dp

    protected fun initToolbarNavigation(toolbar: Toolbar) {
        when (arguments?.getString(ARG_TOOLBAR_ACTION, TOOLBAR_ACTION_BACK)) {
            TOOLBAR_ACTION_MENU -> {
                toolbar.setNavigationIcon(menuIcon)
                toolbar.setNavigationOnClickListener { router.openDrawer() }
            }
            TOOLBAR_ACTION_BACK -> {
                toolbar.setNavigationIcon(backIcon)
                toolbar.setNavigationOnClickListener { router.exit() }
            }
        }
    }

    protected fun initToolbarNavigation(navigationImageView: ImageView) {
        when (arguments?.getString(ARG_TOOLBAR_ACTION, TOOLBAR_ACTION_BACK)) {
            TOOLBAR_ACTION_MENU -> {
                navigationImageView.setImageResource(menuIcon)
                navigationImageView.setOnClickListener { router.openDrawer() }
            }
            TOOLBAR_ACTION_BACK -> {
                navigationImageView.setImageResource(backIcon)
                navigationImageView.setOnClickListener { router.exit() }
            }
        }
    }
}