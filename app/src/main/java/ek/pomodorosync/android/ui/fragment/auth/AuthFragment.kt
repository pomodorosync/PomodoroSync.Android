package ek.pomodorosync.android.ui.fragment.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import ek.pomodorosync.R
import ek.pomodorosync.android.App
import ek.pomodorosync.android.ui.fragment.BaseFragment
import ek.pomodorosync.common.presentation.auth.AuthPresenter
import ek.pomodorosync.common.presentation.auth.AuthView
import kotlinx.android.synthetic.main.fragment_auth.*
import javax.inject.Inject
import javax.inject.Provider

class AuthFragment : BaseFragment(), AuthView {

    @InjectPresenter
    lateinit var presenter: AuthPresenter

    //region Presenter injection
    @Inject lateinit var presenterProvider: Provider<AuthPresenter>

    @ProvidePresenter fun providePresenter() = presenterProvider.get()!!
    //endregion

    override fun onCreate(savedInstanceState: Bundle?) {
        App.instance.appComponent.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_auth, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //initToolbarNavigation(toolbar)
        //toolbar.title = "Авторизация"

        initViews()
    }

    private fun initViews() {
        btnLoginAnonymously.setOnClickListener {
            presenter.loginAnonymously()
        }
        btnLogin.setOnClickListener {
            presenter.login(etEmail.text.toString(), etPassword.text.toString())
        }
        btnSignUp.setOnClickListener {
            presenter.signUp(etEmail.text.toString(), etPassword.text.toString())
        }
    }

    override fun setLoading(isLoading: Boolean) {
        loadingView.visibility = if (isLoading) View.VISIBLE else View.GONE
    }
}