package ek.pomodorosync.android.ui.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.Gravity
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import ek.pomodorosync.R
import ek.pomodorosync.android.App
import ek.pomodorosync.android.ui.fragment.BaseFragment
import ek.pomodorosync.android.ui.fragment.auth.AuthFragment
import ek.pomodorosync.android.ui.fragment.reports.ReportsFragment
import ek.pomodorosync.android.ui.fragment.tasks.AddTaskFragment
import ek.pomodorosync.android.ui.fragment.tasks.TaskListFragment
import ek.pomodorosync.android.ui.fragment.timer.TimerFragment
import ek.pomodorosync.common.AppRouter
import ek.pomodorosync.common.Screens
import ek.pomodorosync.common.presentation.main.MainPresenter
import ek.pomodorosync.common.presentation.main.MainView
import kotlinx.android.synthetic.main.activity_main.*
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.commands.*
import javax.inject.Inject
import javax.inject.Provider


class MainActivity : MvpAppCompatActivity(), MainView {

    @Inject lateinit var navigatorHolder: NavigatorHolder

    @Inject lateinit var router: AppRouter

    @InjectPresenter lateinit var presenter: MainPresenter

    //region Presenter injection
    @Inject lateinit var presenterProvider: Provider<MainPresenter>

    @ProvidePresenter fun providePresenter() = presenterProvider.get()!!
    //endregion

    private val navigator = Navigator { command ->
        when (command) {
            is Forward -> {
                val forward = command
                supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.container, createFragment(forward.screenKey, forward.transitionData, true))
                        .addToBackStack(forward.screenKey)
                        .commit()
            }
            is Back -> {
                if (supportFragmentManager.backStackEntryCount > 0) {
                    supportFragmentManager.popBackStackImmediate()
                } else {
                    finish()
                }
            }
            is Replace -> {
                val replace = command
                if (supportFragmentManager.backStackEntryCount > 0) {
                    supportFragmentManager.popBackStackImmediate()
                    supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.container, createFragment(replace.screenKey, replace.transitionData, true))
                            .addToBackStack(replace.screenKey)
                            .commit()
                } else {
                    supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.container, createFragment(replace.screenKey, replace.transitionData, false))
                            .commit()
                }
            }
            is BackTo -> {
                val key = command.screenKey

                if (key == null) {
                    for (i in 0..supportFragmentManager.backStackEntryCount - 1) {
                        supportFragmentManager.popBackStack()
                    }
                    supportFragmentManager.executePendingTransactions()
                } else {
                    var hasScreen = false
                    for (i in 0..supportFragmentManager.backStackEntryCount - 1) {
                        if (key == supportFragmentManager.getBackStackEntryAt(i).name) {
                            supportFragmentManager.popBackStackImmediate(key, 0)
                            hasScreen = true
                            break
                        }
                    }
                    if (!hasScreen) {
                        error("Screen non exist in backstack")
                    }
                }
            }
            is SystemMessage -> {
                Toast.makeText(this@MainActivity, command.message, Toast.LENGTH_SHORT).show()
            }
            is AppRouter.OpenDrawer -> {
                drawerLayout.openDrawer(Gravity.START)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.instance.appComponent.inject(this)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        navigationView.setNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.nav_timer -> {
                    router.newRootScreen(Screens.TIMER)
                }
                R.id.nav_tasks -> {
                    router.newRootScreen(Screens.TASKS)
                }
                R.id.nav_history -> {
                    router.newRootScreen(Screens.AUTH)
                }
                R.id.nav_reports -> {
                    router.newRootScreen(Screens.REPORTS)
                }
            }
            menuItem.isChecked = true

            drawerLayout.closeDrawers()

            true
        }

    }

    override fun onResumeFragments() {
        super.onResumeFragments()

        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()

        super.onPause()
    }

    fun createFragment(screenKey: String, data: Any?, canGoBack: Boolean): Fragment {
        val action = if (canGoBack) BaseFragment.TOOLBAR_ACTION_BACK else BaseFragment.TOOLBAR_ACTION_MENU

        val args = if (data is Bundle) Bundle(data) else Bundle()
        args.putString(BaseFragment.ARG_TOOLBAR_ACTION, action)
        return createFragmentForScreen(screenKey, args)
    }

    fun createFragmentForScreen(screenKey: String, args: Bundle?): Fragment {
        return when (screenKey) {
            Screens.TASKS -> TaskListFragment()
            Screens.ADD_TASK -> AddTaskFragment()
            Screens.AUTH -> AuthFragment() as Fragment
            Screens.TIMER -> TimerFragment()
            Screens.REPORTS -> ReportsFragment()
            else -> error("Unknown screen key: $screenKey")
        }.apply { arguments = args }
    }
}
