package ek.pomodorosync.android.ui.fragment.tasks

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SimpleAdapter
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import ek.pomodorosync.R
import ek.pomodorosync.android.App
import ek.pomodorosync.android.ui.fragment.BaseFragment
import ek.pomodorosync.common.data.models.TaskModel
import ek.pomodorosync.common.presentation.tasks.TaskListPresenter
import ek.pomodorosync.common.presentation.tasks.TaskListView
import kotlinx.android.synthetic.main.fragment_tasks.*
import kotlinx.android.synthetic.main.include_toolbar.*
import javax.inject.Inject
import javax.inject.Provider

class TaskListFragment : BaseFragment(), TaskListView {

    @InjectPresenter
    lateinit var presenter: TaskListPresenter

    //region Presenter injection
    @Inject lateinit var presenterProvider: Provider<TaskListPresenter>

    @ProvidePresenter fun providePresenter() = presenterProvider.get()!!
    //endregion

    override fun onCreate(savedInstanceState: Bundle?) {
        App.instance.appComponent.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_tasks, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbarNavigation(toolbar)
        toolbar.title = "Задачи"
        initViews()
    }

    private fun initViews() {
        etNewTask.setOnEditorActionListener { _, _, _ ->
            presenter.addTask(etNewTask.text.toString())

            true
        }
    }

    override fun setItems(tasks: List<TaskModel>) {
        if (tasks.isEmpty()) {
            panelEmptyList.visibility = View.VISIBLE
            lvTasks.visibility = View.GONE
        } else {
            panelEmptyList.visibility = View.GONE
            lvTasks.visibility = View.VISIBLE
            lvTasks.adapter = SimpleAdapter(context,
                    tasks.map { mapOf("name" to it.name) },
                    R.layout.item_task,
                    arrayOf("name"),
                    intArrayOf(R.id.tvTitle))
        }
    }
}