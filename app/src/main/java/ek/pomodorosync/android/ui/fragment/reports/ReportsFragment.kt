package ek.pomodorosync.android.ui.fragment.reports

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.github.mikephil.charting.components.XAxis.XAxisPosition
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import ek.pomodorosync.R
import ek.pomodorosync.android.App
import ek.pomodorosync.android.ui.fragment.BaseFragment
import ek.pomodorosync.common.presentation.tasks.AddTaskPresenter
import ek.pomodorosync.common.presentation.tasks.AddTaskView
import kotlinx.android.synthetic.main.fragment_reports.*
import kotlinx.android.synthetic.main.include_toolbar.*
import javax.inject.Inject
import javax.inject.Provider


class ReportsFragment : BaseFragment(), AddTaskView {

    @InjectPresenter
    lateinit var presenter: AddTaskPresenter

    //region Presenter injection
    @Inject lateinit var presenterProvider: Provider<AddTaskPresenter>

    @ProvidePresenter fun providePresenter() = presenterProvider.get()!!
    //endregion

    override fun onCreate(savedInstanceState: Bundle?) {
        App.instance.appComponent.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_reports, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbarNavigation(toolbar)
        toolbar.title = "Статистика"
        initViews()
    }

    private fun initViews() {

        initDaysChart()

        val tags = listOf("Диплом", "Экзамены", "Работа")
        val values = listOf(8, 2, 1)

        val entries = tags.indices.map { PieEntry(values[it].toFloat(), tags[it]) }

        val dataSet = PieDataSet(entries, null)
        dataSet.setDrawValues(false)
        dataSet.setDrawValues(false)

        dataSet.setColors(ColorTemplate.MATERIAL_COLORS, 255)

        chartByTags.data = PieData(dataSet)
        chartByTags.legend.isEnabled = true
        chartByTags.description.isEnabled = false
        chartByTags.setDrawEntryLabels(false)
    }

    private fun initDaysChart() {
        val days = listOf("Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс")
        val values = listOf(2, 6, 3, 2, 4, 0, 2)

        chartByDays.xAxis.apply {
            position = XAxisPosition.BOTTOM
            setDrawGridLines(false)
            setDrawLabels(true)
            setDrawAxisLine(false)
            setDrawLimitLinesBehindData(false)
            granularity = 1f // only intervals of 1 day
            valueFormatter = IAxisValueFormatter { value, _ -> days[value.toInt()] }
        }

        chartByDays.axisLeft?.apply {
            setDrawGridLines(false)
            setDrawLabels(false)
            setDrawAxisLine(false)
            setDrawLimitLinesBehindData(false)
        }
        chartByDays.axisRight?.apply {
            setDrawGridLines(false)
            setDrawLabels(false)
            setDrawAxisLine(false)
            setDrawLimitLinesBehindData(false)
        }

        val entries = days.indices.map { BarEntry(it.toFloat(), values[it].toFloat()) }

        val dataSet = BarDataSet(entries, null)
        dataSet.color = 0xffdd0000.toInt()
        dataSet.valueTextColor = 0x80000000.toInt()
        dataSet.setDrawValues(false)

        chartByDays.data = BarData(dataSet)
        chartByDays.legend.isEnabled = false
        chartByDays.description.isEnabled = false
    }
}