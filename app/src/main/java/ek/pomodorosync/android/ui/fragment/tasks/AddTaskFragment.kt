package ek.pomodorosync.android.ui.fragment.tasks

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import ek.pomodorosync.R
import ek.pomodorosync.android.App
import ek.pomodorosync.android.ui.fragment.BaseFragment
import ek.pomodorosync.common.presentation.tasks.AddTaskPresenter
import ek.pomodorosync.common.presentation.tasks.AddTaskView
import kotlinx.android.synthetic.main.fragment_add_task.*
import javax.inject.Inject
import javax.inject.Provider

class AddTaskFragment : BaseFragment(), AddTaskView {

    @InjectPresenter
    lateinit var presenter: AddTaskPresenter

    //region Presenter injection
    @Inject lateinit var presenterProvider: Provider<AddTaskPresenter>

    @ProvidePresenter fun providePresenter() = presenterProvider.get()!!
    //endregion

    override fun onCreate(savedInstanceState: Bundle?) {
        App.instance.appComponent.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_add_task, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbarNavigation(toolbar)
        toolbar.title = "Добавить задачу"
        initViews()
    }

    private fun initViews() {
        btnDone.setOnClickListener {
            presenter.addTask(etName.text.toString())
        }
    }
}