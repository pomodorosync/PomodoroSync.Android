package ek.pomodorosync.android

import android.app.Application
import com.arellomobile.mvp.RegisterMoxyReflectorPackages
import com.google.firebase.database.FirebaseDatabase
import com.jakewharton.threetenabp.AndroidThreeTen
import ek.pomodorosync.BuildConfig
import ek.pomodorosync.android.dagger.AppComponent
import ek.pomodorosync.android.dagger.AppModule
import ek.pomodorosync.android.dagger.DaggerAppComponent
import ek.pomodorosync.common.util.CommonSchedulers
import io.reactivex.android.schedulers.AndroidSchedulers
import timber.log.Timber

@RegisterMoxyReflectorPackages("common")
class App : Application() {
    companion object {
        lateinit var instance: App
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        AndroidThreeTen.init(this)

        CommonSchedulers.registerMainThreadScheduler(AndroidSchedulers.mainThread())

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        FirebaseDatabase.getInstance().setPersistenceEnabled(true)
    }

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }
}