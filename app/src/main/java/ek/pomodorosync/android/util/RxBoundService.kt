package ek.pomodorosync.android.util

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import io.reactivex.Observable
import io.reactivex.disposables.Disposables

fun <T> Context.rxBindService(
        intent: Intent,
        flag: Int = Context.BIND_AUTO_CREATE,
        binderMapper: (service: IBinder?) -> T
): Observable<T> {
    return Observable.create<T> { emitter ->
        val connection: ServiceConnection?
        connection = object : ServiceConnection {
            override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
                if (!emitter.isDisposed) {
                    emitter.onNext(binderMapper(service))
                }
            }

            override fun onServiceDisconnected(name: ComponentName?) {
                // TODO: handle inter process communication?
            }
        }

        bindService(intent, connection, flag)

        emitter.setDisposable(Disposables.fromAction {
            unbindService(connection)
        })
    }
}
