package ek.pomodorosync.android.util

import org.threeten.bp.Duration
import org.threeten.bp.LocalTime
import org.threeten.bp.format.DateTimeFormatter

fun Duration.format(): String {
    val format = if (this.toHours() > 0) "H:mm:ss" else "mm:ss"

    val time = LocalTime.MIDNIGHT.plus(this)
    return DateTimeFormatter.ofPattern(format).format(time)
}