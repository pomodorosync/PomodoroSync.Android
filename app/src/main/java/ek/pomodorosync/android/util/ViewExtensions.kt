package ek.pomodorosync.android.util

import android.view.View

fun View.setVisible(visible: Boolean, visibilityIfHidden: Int = View.GONE) {
    visibility = if (visible) View.VISIBLE else visibilityIfHidden
}