package ek.pomodorosync.android.util

import com.google.android.gms.tasks.Task
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.Query
import com.google.firebase.database.ValueEventListener
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

fun Query.rxValueChanges(): Observable<DataSnapshot> {
    return Observable.create { emitter ->
        val listener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                emitter.onError(error.toException())
            }

            override fun onDataChange(data: DataSnapshot) {
                emitter.onNext(data)
            }
        }

        addValueEventListener(listener)

        emitter.setCancellable {
            removeEventListener(listener)
        }
    }
}

fun <TResult> Task<TResult>.toSingle(): Single<TResult> {
    return Single.create<TResult> { emitter ->
        addOnCompleteListener { task ->
            if (!emitter.isDisposed) {
                if (task.isSuccessful) {
                    emitter.onSuccess(task.result)
                } else {
                    emitter.onError(task.exception)
                }
            }
        }
    }
}

fun <TResult> Task<TResult>.toCompletable(): Completable {
    return Completable.create { emitter ->
        addOnCompleteListener { task ->
            if (!emitter.isDisposed) {
                if (task.isSuccessful) {
                    emitter.onComplete()
                } else {
                    emitter.onError(task.exception)
                }
            }
        }
    }
}
