package ek.pomodorosync.common.data

import ek.pomodorosync.common.data.models.UserInfo
import io.reactivex.Completable
import io.reactivex.Observable

interface AuthRepository {
    val isLoggedIn: Boolean

    val userId: String?

    val userInfo: UserInfo?

    fun isLoggedInChanges(): Observable<Boolean>

    fun login(email: String, password: String): Completable

    fun loginAnonymously(): Completable

    fun signUp(email: String, password: String): Completable

    fun logout(): Completable
}

