package ek.pomodorosync.common.data

import ek.pomodorosync.common.data.models.TaskModel
import ek.pomodorosync.common.domain.TaskPriority
import io.reactivex.Completable
import io.reactivex.Observable

interface TasksRepository {
    fun getTasks(): Observable<List<TaskModel>>

    fun addTask(name: String, priority: TaskPriority): Completable
}
