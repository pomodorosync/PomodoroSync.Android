package ek.pomodorosync.common.data.models


class TaskModel(val name: String) {

    companion object {
        fun fromMap(map: Map<*, *>): TaskModel {
            return TaskModel(map["name"] as String)
        }
    }

    fun toMap(): Map<String, Any> {
        return mapOf("name" to name)
    }
}
