package ek.pomodorosync.common.data

interface PreferencesRepository {
    var timerLength: Int
    var breakLength: Int

    var autoStartTimer: Boolean
    var autoStartBreak: Boolean

    var isTimerEndNotificationSoundEnabled: Boolean
    var isBreakEndNotificationSoundEnabled: Boolean
}