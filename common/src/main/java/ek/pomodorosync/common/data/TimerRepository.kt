package ek.pomodorosync.common.data

import ek.pomodorosync.common.data.models.TimerState
import io.reactivex.Completable
import io.reactivex.Observable

interface TimerRepository {
    fun getTimerState(): Observable<TimerState>
    fun setTimerState(state: TimerState): Completable

    fun startTimer(duration: Long, isBreak: Boolean = false): Completable
    fun stopTimer(): Completable
}