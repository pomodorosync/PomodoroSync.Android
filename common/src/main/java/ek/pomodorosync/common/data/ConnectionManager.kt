package ek.pomodorosync.common.data

interface ConnectionManager {
    fun isConnected(): Boolean
}
