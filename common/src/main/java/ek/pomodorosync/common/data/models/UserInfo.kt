package ek.pomodorosync.common.data.models

data class UserInfo(
        val name: String?,
        val email: String?,
        val photoUrl: String?)