package ek.pomodorosync.common.domain

enum class TaskPriority {
    LOW, NORMAL, HIGH
}