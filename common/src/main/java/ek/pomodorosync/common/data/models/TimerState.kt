package ek.pomodorosync.common.data.models

data class TimerState(
        val isBreak: Boolean,
        val isRunning: Boolean,
        val startTime: Long,
        val duration: Long
) {
    val endTime = startTime + duration

    companion object {
        fun fromMap(map: Map<*, *>?): TimerState {
            if (map == null) return TimerState(false, false, 0, 1500000)

            return TimerState(
                    map["isBreak"] as Boolean,
                    map["isRunning"] as Boolean,
                    map["startTime"] as Long,
                    map["duration"] as Long)
        }
    }

    fun toMap(): Map<String, Any> {
        return mapOf(
                "isBreak" to isBreak,
                "isRunning" to isRunning,
                "startTime" to startTime,
                "duration" to duration
        )
    }
}