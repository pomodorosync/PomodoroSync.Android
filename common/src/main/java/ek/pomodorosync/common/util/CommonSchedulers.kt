package ek.pomodorosync.common.util

import io.reactivex.Scheduler

object CommonSchedulers {
    private lateinit var mainThreadScheduler: Scheduler

    @JvmStatic
    fun mainThread(): Scheduler {
        return mainThreadScheduler
    }

    @JvmStatic
    fun registerMainThreadScheduler(scheduler: Scheduler) {
        mainThreadScheduler = scheduler
    }
}