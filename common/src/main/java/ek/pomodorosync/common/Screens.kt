package ek.pomodorosync.common

object Screens {
    const val AUTH = "AUTH"
    const val TIMER = "TIMER"
    const val TASKS = "TASKS"
    const val ADD_TASK = "ADD_TASK"
    const val REPORTS = "REPORTS"
}
