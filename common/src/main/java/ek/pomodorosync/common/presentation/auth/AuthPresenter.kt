package ek.pomodorosync.common.presentation.auth

import com.arellomobile.mvp.InjectViewState
import ek.pomodorosync.common.AppRouter
import ek.pomodorosync.common.data.AuthRepository
import ek.pomodorosync.common.presentation.BasePresenter
import ek.pomodorosync.common.util.CommonSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

@InjectViewState
class AuthPresenter @Inject constructor(
        private val authRepository: AuthRepository,
        private val router: AppRouter
) : BasePresenter<AuthView>() {

    fun loginAnonymously() {
        viewState.setLoading(true)
        authRepository.loginAnonymously()
                .subscribeOn(Schedulers.io())
                .observeOn(CommonSchedulers.mainThread())
                .doAfterTerminate {
                    viewState.setLoading(false)
                }
                .subscribeBy(onError = { e ->
                    Timber.w(e, "loginAnonymously error")
                })
                .disposeOnDestroy()
    }

    fun login(login: String, password: String) {
        viewState.setLoading(true)
        authRepository.login(login, password)
                .subscribeOn(Schedulers.io())
                .observeOn(CommonSchedulers.mainThread())
                .doAfterTerminate {
                    viewState.setLoading(false)
                }
                .subscribeBy(onError = { e ->
                    Timber.w(e, "login error")
                })
                .disposeOnDestroy()
    }

    fun signUp(login: String, password: String) {
        viewState.setLoading(true)
        authRepository.signUp(login, password)
                .subscribeOn(Schedulers.io())
                .observeOn(CommonSchedulers.mainThread())
                .doAfterTerminate {
                    viewState.setLoading(false)
                }
                .subscribeBy(onError = { e ->
                    Timber.w(e, "login error")
                })
                .disposeOnDestroy()
    }
}