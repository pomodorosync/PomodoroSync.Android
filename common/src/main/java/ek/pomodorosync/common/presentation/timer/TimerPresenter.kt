package ek.pomodorosync.common.presentation.timer

import com.arellomobile.mvp.InjectViewState
import ek.pomodorosync.common.AppRouter
import ek.pomodorosync.common.data.AuthRepository
import ek.pomodorosync.common.data.models.TimerState
import ek.pomodorosync.common.domain.TimerInteractor
import ek.pomodorosync.common.presentation.BasePresenter
import ek.pomodorosync.common.util.CommonSchedulers
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@InjectViewState
class TimerPresenter @Inject constructor(
        private val timerInteractor: TimerInteractor,
        private val authRepository: AuthRepository,
        private val router: AppRouter
) : BasePresenter<TimerView>() {
    private var currentTimerState: TimerState? = null

    init {
        timerInteractor.getTimerState()
                .observeOn(CommonSchedulers.mainThread())
                .subscribeBy(onNext = { state ->
                    if (state.isRunning) {
                        viewState.setAvailableActions(TimerView.Action.STOP)
                    } else {
                        viewState.setAvailableActions(TimerView.Action.START, TimerView.Action.START_BREAK)
                    }

                    currentTimerState = state
                    updateRemainingTime()
                })

        Observable.interval(1, TimeUnit.SECONDS, CommonSchedulers.mainThread())
                .subscribe { updateRemainingTime() }
                .disposeOnDestroy()
    }

    fun startTimer() {
        timerInteractor.startTimer().subscribe().disposeOnDestroy()
    }

    fun startBreak() {
        timerInteractor.startBreak().subscribe().disposeOnDestroy()
    }

    fun stopTimer() {
        timerInteractor.stop().subscribe().disposeOnDestroy()
    }

    fun logout() {
        authRepository.logout().subscribe().disposeOnDestroy()
    }

    private fun updateRemainingTime() {
        val state = currentTimerState ?: return

        if (state.isRunning) {
            viewState.setRemainingTime(state.endTime - System.currentTimeMillis())
        } else {
            viewState.setRemainingTime(state.duration)
        }
    }
}
