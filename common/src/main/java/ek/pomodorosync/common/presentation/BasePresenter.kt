package ek.pomodorosync.common.presentation

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BasePresenter<View : MvpView> : MvpPresenter<View>() {
    private val onDestroyDisposables = CompositeDisposable()

    override fun onDestroy() {
        onDestroyDisposables.clear()
        super.onDestroy()
    }

    fun Disposable.disposeOnDestroy(): Disposable = apply {
        onDestroyDisposables.add(this)
    }
}