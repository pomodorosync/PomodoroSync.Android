package ek.pomodorosync.common.presentation.timer

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface TimerView : MvpView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setRemainingTime(time: Long)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setAvailableActions(vararg availableActions: Action)

    enum class Action {
        START,
        START_BREAK,
        //PAUSE,
        //RESUME,
        STOP
    }
}