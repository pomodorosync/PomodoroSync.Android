package ek.pomodorosync.common.presentation.tasks

import com.arellomobile.mvp.MvpView
import ek.pomodorosync.common.data.models.TaskModel

interface TaskListView : MvpView {
    fun setItems(tasks: List<TaskModel>)
}