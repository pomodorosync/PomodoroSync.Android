package ek.pomodorosync.common.presentation.auth

import com.arellomobile.mvp.MvpView

interface AuthView : MvpView {
    fun setLoading(isLoading: Boolean)
}