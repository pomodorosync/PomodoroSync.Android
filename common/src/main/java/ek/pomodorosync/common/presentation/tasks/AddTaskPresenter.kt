package ek.pomodorosync.common.presentation.tasks

import com.arellomobile.mvp.InjectViewState
import ek.pomodorosync.common.AppRouter
import ek.pomodorosync.common.domain.TaskPriority
import ek.pomodorosync.common.domain.TasksInteractor
import ek.pomodorosync.common.domain.models.AddTaskRequestModel
import ek.pomodorosync.common.presentation.BasePresenter
import ek.pomodorosync.common.util.CommonSchedulers
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject

@InjectViewState
class AddTaskPresenter @Inject constructor(
        private val tasksInteractor: TasksInteractor,
        private val router: AppRouter
) : BasePresenter<AddTaskView>() {

    fun addTask(name: String) {
        tasksInteractor.addTask(AddTaskRequestModel(name, TaskPriority.NORMAL))
                .observeOn(CommonSchedulers.mainThread())
                .subscribeBy(onComplete = {
                    router.exit()
                }, onError = { e ->
                    Timber.e(e)
                    router.showSystemMessage(e.localizedMessage)
                })
                .disposeOnDestroy()
    }
}