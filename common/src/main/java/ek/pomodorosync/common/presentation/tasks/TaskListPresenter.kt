package ek.pomodorosync.common.presentation.tasks

import com.arellomobile.mvp.InjectViewState
import ek.pomodorosync.common.AppRouter
import ek.pomodorosync.common.data.TasksRepository
import ek.pomodorosync.common.domain.TaskPriority
import ek.pomodorosync.common.presentation.BasePresenter
import ek.pomodorosync.common.util.CommonSchedulers
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject

@InjectViewState
class TaskListPresenter @Inject constructor(
        private val router: AppRouter,
        private val tasksInteractor: TasksRepository
) : BasePresenter<TaskListView>() {

    override fun onFirstViewAttach() {

        tasksInteractor.getTasks()
                .observeOn(CommonSchedulers.mainThread())
                .subscribeBy(onNext = { tasks ->
                    viewState.setItems(tasks)
                }, onError = { e ->
                    Timber.e(e)
                })
                .disposeOnDestroy()
    }

    fun addTask(name: String) {
        tasksInteractor.addTask(name, TaskPriority.NORMAL)
                .observeOn(CommonSchedulers.mainThread())
                .subscribeBy(onComplete = {
                    // do nothing
                }, onError = { e ->
                    Timber.e(e)
                    router.showSystemMessage(e.localizedMessage)
                })
                .disposeOnDestroy()
    }
}

