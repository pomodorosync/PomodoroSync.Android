package ek.pomodorosync.common.presentation.main

import com.arellomobile.mvp.InjectViewState
import ek.pomodorosync.common.AppRouter
import ek.pomodorosync.common.Screens
import ek.pomodorosync.common.data.AuthRepository
import ek.pomodorosync.common.presentation.BasePresenter
import ek.pomodorosync.common.util.CommonSchedulers
import timber.log.Timber
import javax.inject.Inject

@InjectViewState
class MainPresenter @Inject constructor(
        private val authRepository: AuthRepository,
        private val router: AppRouter
) : BasePresenter<MainView>() {

    override fun onFirstViewAttach() {
        Timber.i("logged in: ${authRepository.isLoggedIn}")
        authRepository.isLoggedInChanges()
                .observeOn(CommonSchedulers.mainThread())
                .subscribe { isLoggedIn ->
                    if (isLoggedIn) {
                        router.newRootScreen(Screens.TIMER)
                    } else {
                        router.newRootScreen(Screens.AUTH)
                    }
                }
                .disposeOnDestroy()

    }
}