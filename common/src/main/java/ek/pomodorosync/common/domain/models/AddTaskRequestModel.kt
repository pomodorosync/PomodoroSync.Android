package ek.pomodorosync.common.domain.models

import ek.pomodorosync.common.domain.TaskPriority

data class AddTaskRequestModel(val name: String, val priority: TaskPriority)