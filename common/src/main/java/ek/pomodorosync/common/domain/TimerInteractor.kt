package ek.pomodorosync.common.domain

import ek.pomodorosync.common.data.TimerRepository
import javax.inject.Inject

class TimerInteractor @Inject constructor(
        private val timerRepository: TimerRepository
) {
    fun getTimerState() = timerRepository.getTimerState()

    fun startTimer() = timerRepository.startTimer(25 * 60 * 1000)
    fun startBreak() = timerRepository.startTimer(5 * 60 * 1000, isBreak = true)
    fun startLongBreak() = timerRepository.startTimer(20 * 60 * 1000, isBreak = true)
    fun stop() = timerRepository.stopTimer()
}