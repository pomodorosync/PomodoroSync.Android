package ek.pomodorosync.common.domain

import ek.pomodorosync.common.data.TasksRepository
import ek.pomodorosync.common.domain.models.AddTaskRequestModel
import io.reactivex.Completable
import javax.inject.Inject

class TasksInteractor @Inject constructor(
        private val tasksRepository: TasksRepository
) {
    fun getTasksList() = tasksRepository.getTasks()

    fun addTask(requestModel: AddTaskRequestModel): Completable {
        return tasksRepository.addTask(requestModel.name, requestModel.priority)
    }
}

