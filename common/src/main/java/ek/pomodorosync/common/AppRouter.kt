package ek.pomodorosync.common

import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.commands.Command

class AppRouter : Router() {

    fun openDrawer() {
        executeCommand(OpenDrawer)

    }

    object OpenDrawer : Command
}
