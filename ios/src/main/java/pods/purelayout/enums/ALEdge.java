package pods.purelayout.enums;


import org.moe.natj.general.ann.Generated;
import org.moe.natj.general.ann.NInt;

@Generated
public final class ALEdge {
    @Generated
    @NInt
    public static final long Left = 0x0000000000000001L;
    @Generated
    @NInt
    public static final long Right = 0x0000000000000002L;
    @Generated
    @NInt
    public static final long Top = 0x0000000000000003L;
    @Generated
    @NInt
    public static final long Bottom = 0x0000000000000004L;
    @Generated
    @NInt
    public static final long Leading = 0x0000000000000005L;
    @Generated
    @NInt
    public static final long Trailing = 0x0000000000000006L;

    @Generated
    private ALEdge() {
    }
}