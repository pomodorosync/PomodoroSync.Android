package pods.purelayout.enums;


import org.moe.natj.general.ann.Generated;
import org.moe.natj.general.ann.NInt;

@Generated
public final class ALMargin {
    @Generated
    @NInt
    public static final long Left = 0x000000000000000DL;
    @Generated
    @NInt
    public static final long Right = 0x000000000000000EL;
    @Generated
    @NInt
    public static final long Top = 0x000000000000000FL;
    @Generated
    @NInt
    public static final long Bottom = 0x0000000000000010L;
    @Generated
    @NInt
    public static final long Leading = 0x0000000000000011L;
    @Generated
    @NInt
    public static final long Trailing = 0x0000000000000012L;

    @Generated
    private ALMargin() {
    }
}