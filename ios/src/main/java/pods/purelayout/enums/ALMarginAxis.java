package pods.purelayout.enums;


import org.moe.natj.general.ann.Generated;
import org.moe.natj.general.ann.NInt;

@Generated
public final class ALMarginAxis {
    @Generated
    @NInt
    public static final long Vertical = 0x0000000000000013L;
    @Generated
    @NInt
    public static final long Horizontal = 0x0000000000000014L;

    @Generated
    private ALMarginAxis() {
    }
}