package pods.purelayout.enums;


import org.moe.natj.general.ann.Generated;
import org.moe.natj.general.ann.NInt;

@Generated
public final class ALAxis {
    @Generated
    @NInt
    public static final long Vertical = 0x0000000000000009L;
    @Generated
    @NInt
    public static final long Horizontal = 0x000000000000000AL;
    @Generated
    @NInt
    public static final long Baseline = 0x000000000000000BL;
    @Generated
    @NInt
    public static final long LastBaseline = 0x000000000000000BL;
    @Generated
    @NInt
    public static final long FirstBaseline = 0x000000000000000CL;

    @Generated
    private ALAxis() {
    }
}