package pods.purelayout.enums;


import org.moe.natj.general.ann.Generated;
import org.moe.natj.general.ann.NInt;

@Generated
public final class ALDimension {
    @Generated
    @NInt
    public static final long Width = 0x0000000000000007L;
    @Generated
    @NInt
    public static final long Height = 0x0000000000000008L;

    @Generated
    private ALDimension() {
    }
}