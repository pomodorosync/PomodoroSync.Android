package pods.purelayout.enums;


import org.moe.natj.general.ann.Generated;
import org.moe.natj.general.ann.NInt;

@Generated
public final class ALAttribute {
    @Generated
    @NInt
    public static final long Left = 0x0000000000000001L;
    @Generated
    @NInt
    public static final long Right = 0x0000000000000002L;
    @Generated
    @NInt
    public static final long Top = 0x0000000000000003L;
    @Generated
    @NInt
    public static final long Bottom = 0x0000000000000004L;
    @Generated
    @NInt
    public static final long Leading = 0x0000000000000005L;
    @Generated
    @NInt
    public static final long Trailing = 0x0000000000000006L;
    @Generated
    @NInt
    public static final long Width = 0x0000000000000007L;
    @Generated
    @NInt
    public static final long Height = 0x0000000000000008L;
    @Generated
    @NInt
    public static final long Vertical = 0x0000000000000009L;
    @Generated
    @NInt
    public static final long Horizontal = 0x000000000000000AL;
    @Generated
    @NInt
    public static final long Baseline = 0x000000000000000BL;
    @Generated
    @NInt
    public static final long LastBaseline = 0x000000000000000BL;
    @Generated
    @NInt
    public static final long FirstBaseline = 0x000000000000000CL;
    @Generated
    @NInt
    public static final long MarginLeft = 0x000000000000000DL;
    @Generated
    @NInt
    public static final long MarginRight = 0x000000000000000EL;
    @Generated
    @NInt
    public static final long MarginTop = 0x000000000000000FL;
    @Generated
    @NInt
    public static final long MarginBottom = 0x0000000000000010L;
    @Generated
    @NInt
    public static final long MarginLeading = 0x0000000000000011L;
    @Generated
    @NInt
    public static final long MarginTrailing = 0x0000000000000012L;
    @Generated
    @NInt
    public static final long MarginAxisVertical = 0x0000000000000013L;
    @Generated
    @NInt
    public static final long MarginAxisHorizontal = 0x0000000000000014L;

    @Generated
    private ALAttribute() {
    }
}