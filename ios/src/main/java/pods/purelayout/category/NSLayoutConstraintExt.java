package pods.purelayout.category;


import org.moe.natj.general.NatJ;
import org.moe.natj.general.ann.Generated;
import org.moe.natj.general.ann.MappedReturn;
import org.moe.natj.general.ann.Runtime;
import org.moe.natj.objc.ObjCRuntime;
import org.moe.natj.objc.ann.CategoryClassMethod;
import org.moe.natj.objc.ann.ObjCBlock;
import org.moe.natj.objc.ann.ObjCCategory;
import org.moe.natj.objc.ann.Selector;
import org.moe.natj.objc.map.ObjCObjectMapper;

import apple.foundation.NSArray;
import apple.uikit.NSLayoutConstraint;

@Generated
@Runtime(ObjCRuntime.class)
@ObjCCategory(NSLayoutConstraint.class)
public final class NSLayoutConstraintExt {
    static {
        NatJ.register();
    }

    @Generated
    private NSLayoutConstraintExt() {
    }

    @Generated
    @Selector("autoCreateAndInstallConstraints:")
    @CategoryClassMethod
    public static native NSArray<? extends NSLayoutConstraint> autoCreateAndInstallConstraints(
            @ObjCBlock(name = "call_autoCreateAndInstallConstraints") Block_autoCreateAndInstallConstraints block);

    @Generated
    @Selector("autoCreateConstraintsWithoutInstalling:")
    @CategoryClassMethod
    public static native NSArray<? extends NSLayoutConstraint> autoCreateConstraintsWithoutInstalling(
            @ObjCBlock(name = "call_autoCreateConstraintsWithoutInstalling") Block_autoCreateConstraintsWithoutInstalling block);

    @Generated
    @Selector("autoIdentify:")
    @MappedReturn(ObjCObjectMapper.class)
    public static native Object autoIdentify(NSLayoutConstraint _object,
                                             String identifier);

    @Generated
    @Selector("autoInstall")
    public static native void autoInstall(NSLayoutConstraint _object);

    @Generated
    @Selector("autoRemove")
    public static native void autoRemove(NSLayoutConstraint _object);

    @Generated
    @Selector("autoSetIdentifier:forConstraints:")
    @CategoryClassMethod
    public static native void autoSetIdentifierForConstraints(
            String identifier,
            @ObjCBlock(name = "call_autoSetIdentifierForConstraints") Block_autoSetIdentifierForConstraints block);

    @Generated
    @Selector("autoSetPriority:forConstraints:")
    @CategoryClassMethod
    public static native void autoSetPriorityForConstraints(
            float priority,
            @ObjCBlock(name = "call_autoSetPriorityForConstraints") Block_autoSetPriorityForConstraints block);

    @Runtime(ObjCRuntime.class)
    @Generated
    public interface Block_autoCreateAndInstallConstraints {
        @Generated
        void call_autoCreateAndInstallConstraints();
    }

    @Runtime(ObjCRuntime.class)
    @Generated
    public interface Block_autoCreateConstraintsWithoutInstalling {
        @Generated
        void call_autoCreateConstraintsWithoutInstalling();
    }

    @Runtime(ObjCRuntime.class)
    @Generated
    public interface Block_autoSetIdentifierForConstraints {
        @Generated
        void call_autoSetIdentifierForConstraints();
    }

    @Runtime(ObjCRuntime.class)
    @Generated
    public interface Block_autoSetPriorityForConstraints {
        @Generated
        void call_autoSetPriorityForConstraints();
    }
}