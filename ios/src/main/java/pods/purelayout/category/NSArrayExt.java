package pods.purelayout.category;


import org.moe.natj.general.NatJ;
import org.moe.natj.general.ann.ByValue;
import org.moe.natj.general.ann.Generated;
import org.moe.natj.general.ann.MappedReturn;
import org.moe.natj.general.ann.NFloat;
import org.moe.natj.general.ann.NInt;
import org.moe.natj.general.ann.Runtime;
import org.moe.natj.objc.ObjCRuntime;
import org.moe.natj.objc.ann.ObjCCategory;
import org.moe.natj.objc.ann.Selector;
import org.moe.natj.objc.map.ObjCObjectMapper;

import apple.coregraphics.struct.CGSize;
import apple.foundation.NSArray;
import apple.uikit.NSLayoutConstraint;

@Generated
@Runtime(ObjCRuntime.class)
@ObjCCategory(NSArray.class)
public final class NSArrayExt {
    static {
        NatJ.register();
    }

    @Generated
    private NSArrayExt() {
    }

    @Generated
    @Selector("autoAlignViewsToAxis:")
    public static native NSArray<? extends NSLayoutConstraint> autoAlignViewsToAxis(
            NSArray<?> _object, @NInt long axis);

    @Generated
    @Selector("autoAlignViewsToEdge:")
    public static native NSArray<? extends NSLayoutConstraint> autoAlignViewsToEdge(
            NSArray<?> _object, @NInt long edge);

    @Generated
    @Selector("autoDistributeViewsAlongAxis:alignedTo:withFixedSize:")
    public static native NSArray<? extends NSLayoutConstraint> autoDistributeViewsAlongAxisAlignedToWithFixedSize(
            NSArray<?> _object, @NInt long axis, @NInt long alignment,
            @NFloat double size);

    @Generated
    @Selector("autoDistributeViewsAlongAxis:alignedTo:withFixedSize:insetSpacing:")
    public static native NSArray<? extends NSLayoutConstraint> autoDistributeViewsAlongAxisAlignedToWithFixedSizeInsetSpacing(
            NSArray<?> _object, @NInt long axis, @NInt long alignment,
            @NFloat double size, boolean shouldSpaceInsets);

    @Generated
    @Selector("autoDistributeViewsAlongAxis:alignedTo:withFixedSpacing:")
    public static native NSArray<? extends NSLayoutConstraint> autoDistributeViewsAlongAxisAlignedToWithFixedSpacing(
            NSArray<?> _object, @NInt long axis, @NInt long alignment,
            @NFloat double spacing);

    @Generated
    @Selector("autoDistributeViewsAlongAxis:alignedTo:withFixedSpacing:insetSpacing:")
    public static native NSArray<? extends NSLayoutConstraint> autoDistributeViewsAlongAxisAlignedToWithFixedSpacingInsetSpacing(
            NSArray<?> _object, @NInt long axis, @NInt long alignment,
            @NFloat double spacing, boolean shouldSpaceInsets);

    @Generated
    @Selector("autoDistributeViewsAlongAxis:alignedTo:withFixedSpacing:insetSpacing:matchedSizes:")
    public static native NSArray<? extends NSLayoutConstraint> autoDistributeViewsAlongAxisAlignedToWithFixedSpacingInsetSpacingMatchedSizes(
            NSArray<?> _object, @NInt long axis, @NInt long alignment,
            @NFloat double spacing, boolean shouldSpaceInsets,
            boolean shouldMatchSizes);

    @Generated
    @Selector("autoIdentifyConstraints:")
    @MappedReturn(ObjCObjectMapper.class)
    public static native Object autoIdentifyConstraints(NSArray<?> _object,
                                                        String identifier);

    @Generated
    @Selector("autoInstallConstraints")
    public static native void autoInstallConstraints(NSArray<?> _object);

    @Generated
    @Selector("autoMatchViewsDimension:")
    public static native NSArray<? extends NSLayoutConstraint> autoMatchViewsDimension(
            NSArray<?> _object, @NInt long dimension);

    @Generated
    @Selector("autoRemoveConstraints")
    public static native void autoRemoveConstraints(NSArray<?> _object);

    @Generated
    @Selector("autoSetViewsDimension:toSize:")
    public static native NSArray<? extends NSLayoutConstraint> autoSetViewsDimensionToSize(
            NSArray<?> _object, @NInt long dimension, @NFloat double size);

    @Generated
    @Selector("autoSetViewsDimensionsToSize:")
    public static native NSArray<? extends NSLayoutConstraint> autoSetViewsDimensionsToSize(
            NSArray<?> _object, @ByValue CGSize size);
}