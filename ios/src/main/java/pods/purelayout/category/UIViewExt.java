package pods.purelayout.category;


import org.moe.natj.general.NatJ;
import org.moe.natj.general.ann.ByValue;
import org.moe.natj.general.ann.Generated;
import org.moe.natj.general.ann.MappedReturn;
import org.moe.natj.general.ann.NFloat;
import org.moe.natj.general.ann.NInt;
import org.moe.natj.general.ann.Owned;
import org.moe.natj.general.ann.Runtime;
import org.moe.natj.objc.ObjCRuntime;
import org.moe.natj.objc.ann.CategoryClassMethod;
import org.moe.natj.objc.ann.ObjCCategory;
import org.moe.natj.objc.ann.Selector;
import org.moe.natj.objc.map.ObjCObjectMapper;

import apple.coregraphics.struct.CGSize;
import apple.foundation.NSArray;
import apple.uikit.NSLayoutConstraint;
import apple.uikit.UIView;
import apple.uikit.UIViewController;
import apple.uikit.struct.UIEdgeInsets;

@Generated
@Runtime(ObjCRuntime.class)
@ObjCCategory(UIView.class)
public final class UIViewExt {
    static {
        NatJ.register();
    }

    @Generated
    private UIViewExt() {
    }

    @Generated
    @Selector("autoAlignAxis:toSameAxisOfView:")
    public static native NSLayoutConstraint autoAlignAxisToSameAxisOfView(
            UIView _object, @NInt long axis, UIView otherView);

    @Generated
    @Selector("autoAlignAxis:toSameAxisOfView:withMultiplier:")
    public static native NSLayoutConstraint autoAlignAxisToSameAxisOfViewWithMultiplier(
            UIView _object, @NInt long axis, UIView otherView,
            @NFloat double multiplier);

    @Generated
    @Selector("autoAlignAxis:toSameAxisOfView:withOffset:")
    public static native NSLayoutConstraint autoAlignAxisToSameAxisOfViewWithOffset(
            UIView _object, @NInt long axis, UIView otherView,
            @NFloat double offset);

    @Generated
    @Selector("autoAlignAxisToSuperviewAxis:")
    public static native NSLayoutConstraint autoAlignAxisToSuperviewAxis(
            UIView _object, @NInt long axis);

    @Generated
    @Selector("autoAlignAxisToSuperviewMarginAxis:")
    public static native NSLayoutConstraint autoAlignAxisToSuperviewMarginAxis(
            UIView _object, @NInt long axis);

    @Generated
    @Selector("autoCenterInSuperview")
    public static native NSArray<? extends NSLayoutConstraint> autoCenterInSuperview(
            UIView _object);

    @Generated
    @Selector("autoCenterInSuperviewMargins")
    public static native NSArray<? extends NSLayoutConstraint> autoCenterInSuperviewMargins(
            UIView _object);

    @Generated
    @Selector("autoConstrainAttribute:toAttribute:ofView:")
    public static native NSLayoutConstraint autoConstrainAttributeToAttributeOfView(
            UIView _object, @NInt long attribute, @NInt long toAttribute,
            UIView otherView);

    @Generated
    @Selector("autoConstrainAttribute:toAttribute:ofView:withMultiplier:")
    public static native NSLayoutConstraint autoConstrainAttributeToAttributeOfViewWithMultiplier(
            UIView _object, @NInt long attribute, @NInt long toAttribute,
            UIView otherView, @NFloat double multiplier);

    @Generated
    @Selector("autoConstrainAttribute:toAttribute:ofView:withMultiplier:relation:")
    public static native NSLayoutConstraint autoConstrainAttributeToAttributeOfViewWithMultiplierRelation(
            UIView _object, @NInt long attribute, @NInt long toAttribute,
            UIView otherView, @NFloat double multiplier, @NInt long relation);

    @Generated
    @Selector("autoConstrainAttribute:toAttribute:ofView:withOffset:")
    public static native NSLayoutConstraint autoConstrainAttributeToAttributeOfViewWithOffset(
            UIView _object, @NInt long attribute, @NInt long toAttribute,
            UIView otherView, @NFloat double offset);

    @Generated
    @Selector("autoConstrainAttribute:toAttribute:ofView:withOffset:relation:")
    public static native NSLayoutConstraint autoConstrainAttributeToAttributeOfViewWithOffsetRelation(
            UIView _object, @NInt long attribute, @NInt long toAttribute,
            UIView otherView, @NFloat double offset, @NInt long relation);

    @Generated
    @Selector("autoMatchDimension:toDimension:ofView:")
    public static native NSLayoutConstraint autoMatchDimensionToDimensionOfView(
            UIView _object, @NInt long dimension, @NInt long toDimension,
            UIView otherView);

    @Generated
    @Selector("autoMatchDimension:toDimension:ofView:withMultiplier:")
    public static native NSLayoutConstraint autoMatchDimensionToDimensionOfViewWithMultiplier(
            UIView _object, @NInt long dimension, @NInt long toDimension,
            UIView otherView, @NFloat double multiplier);

    @Generated
    @Selector("autoMatchDimension:toDimension:ofView:withMultiplier:relation:")
    public static native NSLayoutConstraint autoMatchDimensionToDimensionOfViewWithMultiplierRelation(
            UIView _object, @NInt long dimension, @NInt long toDimension,
            UIView otherView, @NFloat double multiplier, @NInt long relation);

    @Generated
    @Selector("autoMatchDimension:toDimension:ofView:withOffset:")
    public static native NSLayoutConstraint autoMatchDimensionToDimensionOfViewWithOffset(
            UIView _object, @NInt long dimension, @NInt long toDimension,
            UIView otherView, @NFloat double offset);

    @Generated
    @Selector("autoMatchDimension:toDimension:ofView:withOffset:relation:")
    public static native NSLayoutConstraint autoMatchDimensionToDimensionOfViewWithOffsetRelation(
            UIView _object, @NInt long dimension, @NInt long toDimension,
            UIView otherView, @NFloat double offset, @NInt long relation);

    @Generated
    @Selector("autoPinEdge:toEdge:ofView:")
    public static native NSLayoutConstraint autoPinEdgeToEdgeOfView(
            UIView _object, @NInt long edge, @NInt long toEdge, UIView otherView);

    @Generated
    @Selector("autoPinEdge:toEdge:ofView:withOffset:")
    public static native NSLayoutConstraint autoPinEdgeToEdgeOfViewWithOffset(
            UIView _object, @NInt long edge, @NInt long toEdge,
            UIView otherView, @NFloat double offset);

    @Generated
    @Selector("autoPinEdge:toEdge:ofView:withOffset:relation:")
    public static native NSLayoutConstraint autoPinEdgeToEdgeOfViewWithOffsetRelation(
            UIView _object, @NInt long edge, @NInt long toEdge,
            UIView otherView, @NFloat double offset, @NInt long relation);

    @Generated
    @Selector("autoPinEdgeToSuperviewEdge:")
    public static native NSLayoutConstraint autoPinEdgeToSuperviewEdge(
            UIView _object, @NInt long edge);

    @Generated
    @Selector("autoPinEdgeToSuperviewEdge:withInset:")
    public static native NSLayoutConstraint autoPinEdgeToSuperviewEdgeWithInset(
            UIView _object, @NInt long edge, @NFloat double inset);

    @Generated
    @Selector("autoPinEdgeToSuperviewEdge:withInset:relation:")
    public static native NSLayoutConstraint autoPinEdgeToSuperviewEdgeWithInsetRelation(
            UIView _object, @NInt long edge, @NFloat double inset,
            @NInt long relation);

    @Generated
    @Selector("autoPinEdgeToSuperviewMargin:")
    public static native NSLayoutConstraint autoPinEdgeToSuperviewMargin(
            UIView _object, @NInt long edge);

    @Generated
    @Selector("autoPinEdgeToSuperviewMargin:relation:")
    public static native NSLayoutConstraint autoPinEdgeToSuperviewMarginRelation(
            UIView _object, @NInt long edge, @NInt long relation);

    @Generated
    @Selector("autoPinEdgesToSuperviewEdges")
    public static native NSArray<? extends NSLayoutConstraint> autoPinEdgesToSuperviewEdges(
            UIView _object);

    @Generated
    @Selector("autoPinEdgesToSuperviewEdgesWithInsets:")
    public static native NSArray<? extends NSLayoutConstraint> autoPinEdgesToSuperviewEdgesWithInsets(
            UIView _object, @ByValue UIEdgeInsets insets);

    @Generated
    @Selector("autoPinEdgesToSuperviewEdgesWithInsets:excludingEdge:")
    public static native NSArray<? extends NSLayoutConstraint> autoPinEdgesToSuperviewEdgesWithInsetsExcludingEdge(
            UIView _object, @ByValue UIEdgeInsets insets, @NInt long edge);

    @Generated
    @Selector("autoPinEdgesToSuperviewMargins")
    public static native NSArray<? extends NSLayoutConstraint> autoPinEdgesToSuperviewMargins(
            UIView _object);

    @Generated
    @Selector("autoPinEdgesToSuperviewMarginsExcludingEdge:")
    public static native NSArray<? extends NSLayoutConstraint> autoPinEdgesToSuperviewMarginsExcludingEdge(
            UIView _object, @NInt long edge);

    @Generated
    @Selector("autoPinToBottomLayoutGuideOfViewController:withInset:")
    public static native NSLayoutConstraint autoPinToBottomLayoutGuideOfViewControllerWithInset(
            UIView _object, UIViewController viewController,
            @NFloat double inset);

    @Generated
    @Selector("autoPinToBottomLayoutGuideOfViewController:withInset:relation:")
    public static native NSLayoutConstraint autoPinToBottomLayoutGuideOfViewControllerWithInsetRelation(
            UIView _object, UIViewController viewController,
            @NFloat double inset, @NInt long relation);

    @Generated
    @Selector("autoPinToTopLayoutGuideOfViewController:withInset:")
    public static native NSLayoutConstraint autoPinToTopLayoutGuideOfViewControllerWithInset(
            UIView _object, UIViewController viewController,
            @NFloat double inset);

    @Generated
    @Selector("autoPinToTopLayoutGuideOfViewController:withInset:relation:")
    public static native NSLayoutConstraint autoPinToTopLayoutGuideOfViewControllerWithInsetRelation(
            UIView _object, UIViewController viewController,
            @NFloat double inset, @NInt long relation);

    @Generated
    @Selector("autoSetContentCompressionResistancePriorityForAxis:")
    public static native void autoSetContentCompressionResistancePriorityForAxis(
            UIView _object, @NInt long axis);

    @Generated
    @Selector("autoSetContentHuggingPriorityForAxis:")
    public static native void autoSetContentHuggingPriorityForAxis(
            UIView _object, @NInt long axis);

    @Generated
    @Selector("autoSetDimension:toSize:")
    public static native NSLayoutConstraint autoSetDimensionToSize(
            UIView _object, @NInt long dimension, @NFloat double size);

    @Generated
    @Selector("autoSetDimension:toSize:relation:")
    public static native NSLayoutConstraint autoSetDimensionToSizeRelation(
            UIView _object, @NInt long dimension, @NFloat double size,
            @NInt long relation);

    @Generated
    @Selector("autoSetDimensionsToSize:")
    public static native NSArray<? extends NSLayoutConstraint> autoSetDimensionsToSize(
            UIView _object, @ByValue CGSize size);

    @Generated
    @Selector("configureForAutoLayout")
    @MappedReturn(ObjCObjectMapper.class)
    public static native Object configureForAutoLayout(UIView _object);

    @Generated
    @Selector("initForAutoLayout")
    public static native UIView initForAutoLayout(UIView _object);

    @Generated
    @Owned
    @Selector("newAutoLayoutView")
    @CategoryClassMethod
    @MappedReturn(ObjCObjectMapper.class)
    public static native Object newAutoLayoutView();
}