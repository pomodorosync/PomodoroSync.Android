package ek.pomodorosync.ios

import apple.NSObject
import apple.foundation.NSArray
import apple.foundation.NSDictionary
import apple.foundation.c.Foundation
import apple.uikit.*
import apple.uikit.c.UIKit
import apple.uikit.protocol.UIApplicationDelegate
import com.arellomobile.mvp.RegisterMoxyReflectorPackages
import ek.pomodorosync.common.AppRouter
import ek.pomodorosync.common.Screens
import ek.pomodorosync.common.presentation.main.MainPresenter
import ek.pomodorosync.common.presentation.main.MainView
import ek.pomodorosync.common.util.CommonSchedulers
import ek.pomodorosync.ios.data.IosAuthRepository
import ek.pomodorosync.ios.ui.AuthViewController
import ek.pomodorosync.ios.ui.TimerViewController
import ek.pomodorosync.ios.util.schedulers.IOSSchedulers
import org.moe.natj.general.Pointer
import org.moe.natj.general.ann.RegisterOnStartup
import pods.firebasecore.FIRApp
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.commands.*
import timber.log.Timber
import java.util.*

@RegisterOnStartup
@RegisterMoxyReflectorPackages("common")
class Main private constructor(peer: Pointer) : NSObject(peer), UIApplicationDelegate, MainView {

    //region Window property
    private var window: UIWindow? = null

    override fun window() = window
    override fun setWindow(value: UIWindow?) = run { window = value }
    //endregion

    private val cicerone = Cicerone.create(AppRouter())
    private lateinit var presenter: MainPresenter

    private lateinit var navigationController: UINavigationController

    private val navigator = object : Navigator {
        val backstackNames = Stack<String>()

        override fun applyCommand(command: Command) {
            if (command is Forward) {
                backstackNames.push(command.screenKey)
                navigationController.pushViewControllerAnimated(
                        createViewController(command.screenKey, command.transitionData), true)
            } else if (command is Back) {
                backstackNames.pop()
                if (navigationController.viewControllers().size > 1) {
                    navigationController.popViewControllerAnimated(true)
                } else {
                    exit()
                }
            } else if (command is Replace) {
                val controller = createViewController(command.screenKey, command.transitionData)

                if (navigationController.viewControllers().size > 1) {
                    backstackNames.pop()
                    navigationController.popViewControllerAnimated(false)

                    backstackNames.push(command.screenKey)
                    navigationController.pushViewControllerAnimated(
                            controller, true)
                } else {
                    val controllers = arrayOf(controller).toNSArray()
                    navigationController.setViewControllers(controllers)
                }

            } else if (command is BackTo) {
                if (command.screenKey == null) {
                    backToRoot()
                } else {
                    for (i in navigationController.viewControllers().indices) {
                        if (command.screenKey == backstackNames[i]) break
                        navigationController.popViewControllerAnimated(false)
                    }
                }
            } else if (command is SystemMessage) {
                showSystemMessage(command.message)
            }
        }

        private fun backToRoot() {
            navigationController.popToRootViewControllerAnimated(true)
        }

        fun showSystemMessage(message: String) {
            Foundation.NSLog("Message: $message")
        }

        fun exit() {
            Foundation.NSLog("Warning: Trying to exit application")
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun <T> Array<T>.toNSArray(): NSArray<T> {
        if (this.isNotEmpty()) {
            return NSArray.arrayWithObjects(this[0], *this.sliceArray(1..this.size - 1), null) as NSArray<T>
        } else {
            return NSArray.alloc().init() as NSArray<T>
        }
    }

    override fun applicationDidFinishLaunchingWithOptions(
            application: UIApplication,
            launchOptions: NSDictionary<*, *>?
    ): Boolean {
        CommonSchedulers.registerMainThreadScheduler(IOSSchedulers.mainThread())

        Timber.plant(Timber.DebugTree())

        FIRApp.configure()

        initWindow()

        cicerone.navigatorHolder.setNavigator(navigator)

        presenter = MainPresenter(IosAuthRepository(), cicerone.router)
        presenter.attachView(this)

        return true
    }


    private fun initWindow() {
        navigationController = UINavigationController.alloc().init()

        window = UIWindow.alloc().initWithFrame(UIScreen.mainScreen().bounds()).apply {
            setRootViewController(navigationController)
            setBackgroundColor(UIColor.whiteColor())
        }

        window!!.makeKeyAndVisible()
    }

    private fun createViewController(screenKey: String, transitionData: Any?): UIViewController {
        return when (screenKey) {
            Screens.AUTH -> createViewControllerFromNib<AuthViewController>()
            Screens.TIMER -> createViewControllerFromNib<TimerViewController>()
        //Screens.ADD_TASK -> createViewControllerFromNib<AddTaskViewController>()
            else -> error("Unknown screen key: $screenKey")
        }
    }

    private inline fun <reified T> createViewControllerFromNib(): T {
        return UINib.nibWithNibNameBundle(T::class.java.simpleName, null)
                .instantiateWithOwnerOptions(null, null)[0] as T
    }

    companion object {

        @JvmStatic fun main(args: Array<String>) {
            UIKit.UIApplicationMain(0, null, null, Main::class.java.name)
        }
    }
}
