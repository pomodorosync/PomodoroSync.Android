package ek.pomodorosync.ios.ui

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import org.moe.natj.general.Pointer
import org.moe.natj.general.ann.RegisterOnStartup
import org.moe.natj.objc.ObjCRuntime
import org.moe.natj.objc.ann.ObjCClassName

@org.moe.natj.general.ann.Runtime(ObjCRuntime::class)
@ObjCClassName("BaseCodeLayoutViewController")
@RegisterOnStartup
abstract class BaseCodeLayoutViewController<T : MvpPresenter<V>, V : MvpView> protected constructor(peer: Pointer) : BaseViewController<T, V>(peer) {

    private var didUpdateConstraints = false

    protected abstract fun setupView()
    protected abstract fun setupConstraints()

    override fun viewDidLoad() {

        super.viewDidLoad()

        presenter = createPresenter()
    }

    override fun loadView() {
        setupView()
        view().setNeedsUpdateConstraints()
    }

    override fun updateViewConstraints() {
        if (!didUpdateConstraints) {
            setupConstraints()

            didUpdateConstraints = true;
        }

        super.updateViewConstraints()
    }
}
