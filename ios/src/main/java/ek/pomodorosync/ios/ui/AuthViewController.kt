package ek.pomodorosync.ios.ui

import apple.uikit.UIButton
import apple.uikit.UITextField
import ek.pomodorosync.common.AppRouter
import ek.pomodorosync.common.presentation.auth.AuthPresenter
import ek.pomodorosync.common.presentation.auth.AuthView
import ek.pomodorosync.ios.data.IosAuthRepository
import org.moe.natj.general.Pointer
import org.moe.natj.general.ann.Owned
import org.moe.natj.general.ann.RegisterOnStartup
import org.moe.natj.objc.ObjCRuntime
import org.moe.natj.objc.ann.*

@org.moe.natj.general.ann.Runtime(ObjCRuntime::class)
@ObjCClassName("AuthViewController")
@RegisterOnStartup
class AuthViewController private constructor(peer: Pointer)
    : BaseViewController<AuthPresenter, AuthView>(peer), AuthView {

    @get:IBOutlet @get:Property @get:Selector("loginTextField")
    @set:IBOutlet @set:Selector("setLoginTextField:")
    lateinit var loginTextField: UITextField

    @get:IBOutlet @get:Property @get:Selector("passwordTextField")
    @set:IBOutlet @set:Selector("setPasswordTextField:")
    lateinit var passwordTextField: UITextField

    @IBAction
    @Selector("onLoginClick:")
    fun onLoginClick(view: UIButton?) {
        presenter.login(loginTextField.text(), passwordTextField.text())
    }

    @IBAction
    @Selector("onSkipClick:")
    fun onSkipClick(view: UIButton?) {
        presenter.loginAnonymously()
    }

    override fun createPresenter() = AuthPresenter(IosAuthRepository(), AppRouter())

    override fun viewDidLoad() {
        super.viewDidLoad()

        navigationItem().setTitle("Авторизация")
    }

    //region Allocation
    @Selector("init")
    override external fun init(): AuthViewController

    companion object {

        @Owned
        @Selector("alloc")
        @JvmStatic external fun alloc(): AuthViewController
    }
    //endregion

    override fun setLoading(isLoading: Boolean) {

    }
}
