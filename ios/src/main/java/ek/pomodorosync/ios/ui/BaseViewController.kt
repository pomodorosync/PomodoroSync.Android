package ek.pomodorosync.ios.ui

import apple.uikit.UIViewController
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import org.moe.natj.general.Pointer
import org.moe.natj.general.ann.RegisterOnStartup
import org.moe.natj.objc.ObjCRuntime
import org.moe.natj.objc.ann.ObjCClassName

@org.moe.natj.general.ann.Runtime(ObjCRuntime::class)
@ObjCClassName("BaseViewController")
@RegisterOnStartup
abstract class BaseViewController<T : MvpPresenter<V>, V : MvpView> protected constructor(peer: Pointer) : UIViewController(peer) {

    private var didUpdateConstraints = false

    protected lateinit var presenter: T

    protected abstract fun createPresenter(): T

    override fun viewDidLoad() {

        super.viewDidLoad()

        presenter = createPresenter()
    }

    override fun viewDidAppear(animated: Boolean) {
        super.viewDidAppear(animated)

        @Suppress("UNCHECKED_CAST")
        presenter.attachView(this as V)
    }

    override fun viewWillDisappear(animated: Boolean) {
        @Suppress("UNCHECKED_CAST")
        presenter.detachView(this as V)

        super.viewWillDisappear(animated)
    }

}