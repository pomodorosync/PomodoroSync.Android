package ek.pomodorosync.ios.ui

import apple.uikit.UIButton
import apple.uikit.UILabel
import ek.pomodorosync.common.AppRouter
import ek.pomodorosync.common.data.TimerRepository
import ek.pomodorosync.common.data.models.TimerState
import ek.pomodorosync.common.domain.TimerInteractor
import ek.pomodorosync.common.presentation.timer.TimerPresenter
import ek.pomodorosync.common.presentation.timer.TimerView
import ek.pomodorosync.ios.data.IosAuthRepository
import ek.pomodorosync.ios.util.formatDuration
import io.reactivex.Completable
import io.reactivex.Observable
import org.moe.natj.general.Pointer
import org.moe.natj.general.ann.Owned
import org.moe.natj.general.ann.RegisterOnStartup
import org.moe.natj.objc.ObjCRuntime
import org.moe.natj.objc.ann.*

@org.moe.natj.general.ann.Runtime(ObjCRuntime::class)
@ObjCClassName("TimerViewController")
@RegisterOnStartup
class TimerViewController private constructor(peer: Pointer)
    : BaseViewController<TimerPresenter, TimerView>(peer), TimerView {


    @get:IBOutlet @get:Property @get:Selector("timerLabel")
    @set:IBOutlet @set:Selector("setTimerLabel:")
    lateinit var timerLabel: UILabel

    @get:IBOutlet @get:Property @get:Selector("startDescriptionLabel")
    @set:IBOutlet @set:Selector("setStartDescriptionLabel:")
    lateinit var startDescriptionLabel: UILabel

    @get:IBOutlet @get:Property @get:Selector("startButton")
    @set:IBOutlet @set:Selector("setStartButton:")
    lateinit var startButton: UIButton

    @get:IBOutlet @get:Property @get:Selector("stopButton")
    @set:IBOutlet @set:Selector("setStopButton:")
    lateinit var stopButton: UIButton

    @IBAction
    @Selector("onStartClick:")
    fun onStartClick(view: UIButton?) {
        presenter.startTimer()
    }

    @IBAction
    @Selector("onStopClick:")
    fun onStopClick(view: UIButton?) {
        presenter.stopTimer()
    }

    override fun createPresenter() = TimerPresenter(TimerInteractor(object : TimerRepository {
        override fun getTimerState(): Observable<TimerState> {
            TODO()
        }

        override fun setTimerState(state: TimerState): Completable {
            TODO()
        }

        override fun startTimer(duration: Long, isBreak: Boolean): Completable {
            TODO()
        }

        override fun stopTimer(): Completable {
            TODO()
        }
    }), IosAuthRepository(), AppRouter())

    override fun viewDidLoad() {
        super.viewDidLoad()

    }

    override fun setRemainingTime(time: Long) {
        timerLabel.setText(formatDuration(time / 1000.0))
    }

    override fun setAvailableActions(vararg availableActions: TimerView.Action) {
        for (action in TimerView.Action.values()) {
            val isAvailable = action in availableActions
            when (action) {
                TimerView.Action.START -> {
                    startDescriptionLabel.isHidden = !isAvailable
                    startButton.isHidden = !isAvailable
                    startButton.isEnabled = isAvailable
                }
                TimerView.Action.STOP -> {
                    stopButton.isHidden = !isAvailable
                }
            // else -> error("Unknown action: $action")
            }
        }
    }

    //region Allocation
    @Selector("init")
    override external fun init(): TimerViewController

    companion object {

        @Owned
        @Selector("alloc")
        @JvmStatic external fun alloc(): TimerViewController
    }
    //endregion
}

