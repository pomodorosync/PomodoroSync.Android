package ek.pomodorosync.ios.util

import apple.foundation.NSDateComponentsFormatter
import apple.foundation.enums.NSCalendarUnit
import apple.foundation.enums.NSDateComponentsFormatterUnitsStyle
import apple.foundation.enums.NSDateComponentsFormatterZeroFormattingBehavior

private val durationFormatter = NSDateComponentsFormatter.alloc().init().apply {
    setUnitsStyle(NSDateComponentsFormatterUnitsStyle.Positional)
    setAllowedUnits(NSCalendarUnit.CalendarUnitMinute or NSCalendarUnit.CalendarUnitSecond)
    setZeroFormattingBehavior(NSDateComponentsFormatterZeroFormattingBehavior.Pad)
}

fun formatDuration(seconds: Double): String {
    return durationFormatter.stringFromTimeInterval(seconds)
}