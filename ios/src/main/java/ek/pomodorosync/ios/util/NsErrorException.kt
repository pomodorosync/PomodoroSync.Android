package ek.pomodorosync.ios.util

import apple.foundation.NSError

class NSErrorException(error: NSError) : RuntimeException(error.localizedDescription())