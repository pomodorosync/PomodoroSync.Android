package ek.pomodorosync.ios.util

import apple.coregraphics.struct.CGSize
import apple.foundation.NSArray
import apple.uikit.NSLayoutConstraint
import apple.uikit.UIView
import apple.uikit.UIViewController
import apple.uikit.struct.UIEdgeInsets
import org.moe.natj.general.ann.ByValue
import org.moe.natj.general.ann.NFloat
import org.moe.natj.general.ann.NInt
import pods.purelayout.category.UIViewExt

fun UIView.autoAlignAxisToSameAxisOfView(@NInt axis: Long, otherView: UIView): NSLayoutConstraint {
    return UIViewExt.autoAlignAxisToSameAxisOfView(this, axis, otherView)
}

fun UIView.autoAlignAxisToSameAxisOfViewWithMultiplier(@NInt axis: Long, otherView: UIView, @NFloat multiplier: Double): NSLayoutConstraint {
    return UIViewExt.autoAlignAxisToSameAxisOfViewWithMultiplier(this, axis, otherView, multiplier)
}

fun UIView.autoAlignAxisToSameAxisOfViewWithOffset(@NInt axis: Long, otherView: UIView, @NFloat offset: Double): NSLayoutConstraint {
    return UIViewExt.autoAlignAxisToSameAxisOfViewWithOffset(this, axis, otherView, offset)
}

fun UIView.autoAlignAxisToSuperviewAxis(@NInt axis: Long): NSLayoutConstraint {
    return UIViewExt.autoAlignAxisToSuperviewAxis(this, axis)
}

fun UIView.autoAlignAxisToSuperviewMarginAxis(@NInt axis: Long): NSLayoutConstraint {
    return UIViewExt.autoAlignAxisToSuperviewMarginAxis(this, axis)
}

fun UIView.autoCenterInSuperview(): NSArray<out NSLayoutConstraint> {
    return UIViewExt.autoCenterInSuperview(this)
}

fun UIView.autoCenterInSuperviewMargins(): NSArray<out NSLayoutConstraint> {
    return UIViewExt.autoCenterInSuperviewMargins(this)
}

fun UIView.autoConstrainAttributeToAttributeOfView(@NInt attribute: Long, @NInt toAttribute: Long, otherView: UIView): NSLayoutConstraint {
    return UIViewExt.autoConstrainAttributeToAttributeOfView(this, attribute, toAttribute, otherView)
}

fun UIView.autoConstrainAttributeToAttributeOfViewWithMultiplier(@NInt attribute: Long, @NInt toAttribute: Long, otherView: UIView, @NFloat multiplier: Double): NSLayoutConstraint {
    return UIViewExt.autoConstrainAttributeToAttributeOfViewWithMultiplier(this, attribute, toAttribute, otherView, multiplier)
}

fun UIView.autoConstrainAttributeToAttributeOfViewWithMultiplierRelation(@NInt attribute: Long, @NInt toAttribute: Long, otherView: UIView, @NFloat multiplier: Double, @NInt relation: Long): NSLayoutConstraint {
    return UIViewExt.autoConstrainAttributeToAttributeOfViewWithMultiplierRelation(this, attribute, toAttribute, otherView, multiplier, relation)
}

fun UIView.autoConstrainAttributeToAttributeOfViewWithOffset(@NInt attribute: Long, @NInt toAttribute: Long, otherView: UIView, @NFloat offset: Double): NSLayoutConstraint {
    return UIViewExt.autoConstrainAttributeToAttributeOfViewWithOffset(this, attribute, toAttribute, otherView, offset)
}

fun UIView.autoConstrainAttributeToAttributeOfViewWithOffsetRelation(@NInt attribute: Long, @NInt toAttribute: Long, otherView: UIView, @NFloat offset: Double, @NInt relation: Long): NSLayoutConstraint {
    return UIViewExt.autoConstrainAttributeToAttributeOfViewWithOffsetRelation(this, attribute, toAttribute, otherView, offset, relation)
}

fun UIView.autoMatchDimensionToDimensionOfView(@NInt dimension: Long, @NInt toDimension: Long, otherView: UIView): NSLayoutConstraint {
    return UIViewExt.autoMatchDimensionToDimensionOfView(this, dimension, toDimension, otherView)
}

fun UIView.autoMatchDimensionToDimensionOfViewWithMultiplier(@NInt dimension: Long, @NInt toDimension: Long, otherView: UIView, @NFloat multiplier: Double): NSLayoutConstraint {
    return UIViewExt.autoMatchDimensionToDimensionOfViewWithMultiplier(this, dimension, toDimension, otherView, multiplier)
}

fun UIView.autoMatchDimensionToDimensionOfViewWithMultiplierRelation(@NInt dimension: Long, @NInt toDimension: Long, otherView: UIView, @NFloat multiplier: Double, @NInt relation: Long): NSLayoutConstraint {
    return UIViewExt.autoMatchDimensionToDimensionOfViewWithMultiplierRelation(this, dimension, toDimension, otherView, multiplier, relation)
}

fun UIView.autoMatchDimensionToDimensionOfViewWithOffset(@NInt dimension: Long, @NInt toDimension: Long, otherView: UIView, @NFloat offset: Double): NSLayoutConstraint {
    return UIViewExt.autoMatchDimensionToDimensionOfViewWithOffset(this, dimension, toDimension, otherView, offset)
}

fun UIView.autoMatchDimensionToDimensionOfViewWithOffsetRelation(@NInt dimension: Long, @NInt toDimension: Long, otherView: UIView, @NFloat offset: Double, @NInt relation: Long): NSLayoutConstraint {
    return UIViewExt.autoMatchDimensionToDimensionOfViewWithOffsetRelation(this, dimension, toDimension, otherView, offset, relation)
}

fun UIView.autoPinEdgeToEdgeOfView(@NInt edge: Long, @NInt toEdge: Long, otherView: UIView): NSLayoutConstraint {
    return UIViewExt.autoPinEdgeToEdgeOfView(this, edge, toEdge, otherView)
}

fun UIView.autoPinEdgeToEdgeOfViewWithOffset(@NInt edge: Long, @NInt toEdge: Long, otherView: UIView, @NFloat offset: Double): NSLayoutConstraint {
    return UIViewExt.autoPinEdgeToEdgeOfViewWithOffset(this, edge, toEdge, otherView, offset)
}

fun UIView.autoPinEdgeToEdgeOfViewWithOffsetRelation(@NInt edge: Long, @NInt toEdge: Long, otherView: UIView, @NFloat offset: Double, @NInt relation: Long): NSLayoutConstraint {
    return UIViewExt.autoPinEdgeToEdgeOfViewWithOffsetRelation(this, edge, toEdge, otherView, offset, relation)
}

fun UIView.autoPinEdgeToSuperviewEdge(@NInt edge: Long): NSLayoutConstraint {
    return UIViewExt.autoPinEdgeToSuperviewEdge(this, edge)
}

fun UIView.autoPinEdgeToSuperviewEdgeWithInset(@NInt edge: Long, @NFloat inset: Double): NSLayoutConstraint {
    return UIViewExt.autoPinEdgeToSuperviewEdgeWithInset(this, edge, inset)
}

fun UIView.autoPinEdgeToSuperviewEdgeWithInsetRelation(@NInt edge: Long, @NFloat inset: Double, @NInt relation: Long): NSLayoutConstraint {
    return UIViewExt.autoPinEdgeToSuperviewEdgeWithInsetRelation(this, edge, inset, relation)
}

fun UIView.autoPinEdgeToSuperviewMargin(@NInt edge: Long): NSLayoutConstraint {
    return UIViewExt.autoPinEdgeToSuperviewMargin(this, edge)
}

fun UIView.autoPinEdgeToSuperviewMarginRelation(@NInt edge: Long, @NInt relation: Long): NSLayoutConstraint {
    return UIViewExt.autoPinEdgeToSuperviewMarginRelation(this, edge, relation)
}

fun UIView.autoPinEdgesToSuperviewEdges(): NSArray<out NSLayoutConstraint> {
    return UIViewExt.autoPinEdgesToSuperviewEdges(this)
}

fun UIView.autoPinEdgesToSuperviewEdgesWithInsets(@ByValue insets: UIEdgeInsets): NSArray<out NSLayoutConstraint> {
    return UIViewExt.autoPinEdgesToSuperviewEdgesWithInsets(this, insets)
}

fun UIView.autoPinEdgesToSuperviewEdgesWithInsetsExcludingEdge(@ByValue insets: UIEdgeInsets, @NInt edge: Long): NSArray<out NSLayoutConstraint> {
    return UIViewExt.autoPinEdgesToSuperviewEdgesWithInsetsExcludingEdge(this, insets, edge)
}

fun UIView.autoPinEdgesToSuperviewMargins(): NSArray<out NSLayoutConstraint> {
    return UIViewExt.autoPinEdgesToSuperviewMargins(this)
}

fun UIView.autoPinEdgesToSuperviewMarginsExcludingEdge(@NInt edge: Long): NSArray<out NSLayoutConstraint> {
    return UIViewExt.autoPinEdgesToSuperviewMarginsExcludingEdge(this, edge)
}

fun UIView.autoPinToBottomLayoutGuideOfViewControllerWithInset(viewController: UIViewController, @NFloat inset: Double): NSLayoutConstraint {
    return UIViewExt.autoPinToBottomLayoutGuideOfViewControllerWithInset(this, viewController, inset)
}

fun UIView.autoPinToBottomLayoutGuideOfViewControllerWithInsetRelation(viewController: UIViewController, @NFloat inset: Double, @NInt relation: Long): NSLayoutConstraint {
    return UIViewExt.autoPinToBottomLayoutGuideOfViewControllerWithInsetRelation(this, viewController, inset, relation)
}

fun UIView.autoPinToTopLayoutGuideOfViewControllerWithInset(viewController: UIViewController, @NFloat inset: Double): NSLayoutConstraint {
    return UIViewExt.autoPinToTopLayoutGuideOfViewControllerWithInset(this, viewController, inset)
}

fun UIView.autoPinToTopLayoutGuideOfViewControllerWithInsetRelation(viewController: UIViewController, @NFloat inset: Double, @NInt relation: Long): NSLayoutConstraint {
    return UIViewExt.autoPinToTopLayoutGuideOfViewControllerWithInsetRelation(this, viewController, inset, relation)
}

fun UIView.autoSetContentCompressionResistancePriorityForAxis(@NInt axis: Long) {
    UIViewExt.autoSetContentCompressionResistancePriorityForAxis(this, axis)
}

fun UIView.autoSetContentHuggingPriorityForAxis(@NInt axis: Long) {
    UIViewExt.autoSetContentHuggingPriorityForAxis(this, axis)
}

fun UIView.autoSetDimensionToSize(@NInt dimension: Long, @NFloat size: Double): NSLayoutConstraint {
    return UIViewExt.autoSetDimensionToSize(this, dimension, size)
}

fun UIView.autoSetDimensionToSizeRelation(@NInt dimension: Long, @NFloat size: Double, @NInt relation: Long): NSLayoutConstraint {
    return UIViewExt.autoSetDimensionToSizeRelation(this, dimension, size, relation)
}

fun UIView.autoSetDimensionsToSize(@ByValue size: CGSize): NSArray<out NSLayoutConstraint> {
    return UIViewExt.autoSetDimensionsToSize(this, size)
}

fun UIView.configureForAutoLayout(): Any {
    return UIViewExt.configureForAutoLayout(this)
}