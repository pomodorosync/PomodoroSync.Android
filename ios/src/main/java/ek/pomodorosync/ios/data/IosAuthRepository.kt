package ek.pomodorosync.ios.data

import apple.foundation.NSError
import ek.pomodorosync.common.data.AuthRepository
import ek.pomodorosync.common.data.models.UserInfo
import ek.pomodorosync.ios.util.NSErrorException
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.disposables.Disposables
import org.moe.natj.general.ptr.impl.PtrFactory
import pods.firebaseauth.FIRAuth


class IosAuthRepository : AuthRepository {
    override val isLoggedIn: Boolean
        get() = FIRAuth.auth().currentUser() != null && !FIRAuth.auth().currentUser()!!.isAnonymous

    override val userId: String?
        get() = FIRAuth.auth().currentUser()?.uid()

    override val userInfo: UserInfo?
        get() = FIRAuth.auth().currentUser()?.let {
            UserInfo(it.displayName(), it.email(), it.photoURL().absoluteString())
        }

    override fun isLoggedInChanges(): Observable<Boolean> {
        return Observable.create { emitter ->
            val handle = FIRAuth.auth().addAuthStateDidChangeListener({ _, user ->
                if (!emitter.isDisposed) {
                    emitter.onNext(user != null)
                }
            })

            emitter.setDisposable(Disposables.fromAction {
                FIRAuth.auth().removeAuthStateDidChangeListener(handle)
            })
        }
    }

    override fun login(email: String, password: String): Completable {
        return Completable.create { emitter ->
            FIRAuth.auth().signInWithEmailPasswordCompletion(email, password) { user, error ->
                if (!emitter.isDisposed) {
                    if (user != null) {
                        emitter.onComplete()
                    } else {
                        emitter.onError(Exception(error.localizedDescription()))
                    }
                }
            }
        }
    }

    override fun loginAnonymously(): Completable {
        return Completable.create { emitter ->
            FIRAuth.auth().signInAnonymouslyWithCompletion { user, error ->
                if (!emitter.isDisposed) {
                    if (user != null) {
                        emitter.onComplete()
                    } else {
                        emitter.onError(Exception(error.localizedDescription()))
                    }
                }
            }
        }
    }

    override fun signUp(email: String, password: String): Completable {
        return Completable.create { emitter ->
            FIRAuth.auth().createUserWithEmailPasswordCompletion(email, password) { user, error ->
                if (!emitter.isDisposed) {
                    if (user != null) {
                        emitter.onComplete()
                    } else {
                        emitter.onError(Exception(error.localizedDescription()))
                    }
                }
            }
        }
    }

    override fun logout(): Completable {
        return Completable.create { emitter ->
            val error = PtrFactory.newObjectReference(NSError::class.java)
            val isSuccessful = FIRAuth.auth().signOut(error)

            if (isSuccessful) {
                emitter.onComplete()
            } else {
                emitter.onError(NSErrorException(error.get()))
            }
        }
    }

}